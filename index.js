async function asd() {

    var jose = require('node-jose');


    var privateKey = `
-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIHXfij/l3F97wrIlicVVHh00nn3wMeIUgFuFXfFpbbQ/oAoGCCqGSM49
AwEHoUQDQgAEmI2FhXpDfGyvElormnYwrafLuJyt3vwW9Gv/O6vaaxBB0ppDHmLh
Bf0ImRM5BxFbNTDx8sBtB5IKb4kIlvlv9w==
-----END EC PRIVATE KEY-----
    `;

    var privateKey = await jose.JWK.asKey(privateKey, "pem");
    var payload = JSON.stringify({
        "iss": "Cygate",
        "sub": JSON.stringify({
            "UserID": 123666,
            "Email":"test.user@cygate.fi"
        }),
        "exp": Math.round(new Date().getTime() / 1000) + 3600,
        "iat": Math.round(new Date().getTime() / 1000),
        "jti": Math.random().toString(36).substring(2, 16)
    });

    var jwtSigned = await jose.JWS.createSign({
            format: 'compact'
        }, privateKey)
        .update(payload, "utf8")
        .final();

    console.log('');
    console.log('Signed JWT');
    console.log(jwtSigned);
    console.log('');



    var lekanePublicKey = `
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAED6bX4dgLGJ/xZPkvF/872Ts3AEX2
SgkdIc08rHEbcN47su5qra7/qgVvouvXr/wL2ljoCqqBkCh/eK4UWOrsFg==
-----END PUBLIC KEY-----
        `;
    var lekanePublicKey = await jose.JWK.asKey(lekanePublicKey, "pem");


    var jwtEncrypted = await jose.JWE.createEncrypt({
            format: 'compact',
            fields: {
                alg: 'ECDH-ES+A256KW',
                enc: 'A256CBC-HS512',
            }
        }, lekanePublicKey)
        .update(JSON.stringify(jwtSigned))
        .final();

    console.log('');
    console.log('Encrypted JWT');
    console.log(jwtEncrypted);
    console.log('');


}

asd();